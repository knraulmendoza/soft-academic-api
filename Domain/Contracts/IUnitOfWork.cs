﻿using System;

namespace Domain.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        #region Repositories
        #endregion
        int Commit();
    }
}
